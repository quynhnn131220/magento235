<?php

namespace SU\Blog\Block\Post;

use Magento\Framework\Registry;
use Magento\Framework\UrlInterface as MagentoUrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use SU\Blog\Model\CategoryFactory;
use SU\Blog\Model\CategoryPostFactory;
use SU\Blog\Model\PostFactory;
use SU\Blog\Model\ProductPostFactory;
use SU\Blog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use SU\Blog\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
use SU\Blog\Model\ResourceModel\Tag\CollectionFactory as TagCollectionFactory;

class View extends Template
{
    protected $postCollectionFactory;
    protected $categoryCollectionFactory;
    protected $registry;
    protected $context;
    protected $postFactory;
    protected $storeManager;
    protected $categoryFactory;
    protected $categoryPostFactory;
    protected $tagCollectionFactory;
    protected $_productCollectionFactory;
    protected $productPostFactory;
    const MEDIA_FOLDER = 'catalog/tmp/category';

    public function __construct(
        StoreManagerInterface $storeManager,
        PostCollectionFactory $postCollectionFactory,
        CategoryCollectionFactory $categoryCollectionFactory,
        Registry $registry,
        Context $context,
        PostFactory $postFactory,
        CategoryFactory $categoryFactory,
        CategoryPostFactory $categoryPostFactory,
        TagCollectionFactory $tagCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        ProductPostFactory $productPostFactory,
        array $data = []
    ) {
        $this->postCollectionFactory     = $postCollectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->registry                  = $registry;
        $this->context                   = $context;
        $this->postFactory               = $postFactory;
        $this->storeManager              = $storeManager;
        $this->categoryFactory           = $categoryFactory;
        $this->categoryPostFactory       = $categoryPostFactory;
        $this->tagCollectionFactory      = $tagCollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->imageHelper = $imageHelper;
        $this->productRepository = $productRepository;
        $this->productPostFactory = $productPostFactory;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $idPost = $this->getRequest()->getParam('id');

        $post = $this->postFactory->create()->load($idPost);

        $title = $post->getTitle();

        /** @var Title $pageMainTitle */
        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle($title);
        }

        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb('home', [
                'label' => __('Home'),
                'title' => __('Go to Home Page'),
                'link' => $this->context->getUrlBuilder()->getBaseUrl(),
            ])->addCrumb('blog', [
                'label' => __('Blog'),
                'title' => __('Blog'),
                'link'  => $this->getBaseUrl() . 'blog',
            ]);

            $breadcrumbs->addCrumb($post->getId(), [
                'label' => $post->getTitle(),
                'title' => $post->getTitle()
            ]);
        }

        return $this;
    }

    public function getPost()
    {
        $id = $this->getRequest()->getParam('id');

        $post = $this->postFactory->create()->load($id);

        return $post;
    }

    public function getCategoryPostName($idPost)
    {
        $collection = $this->categoryCollectionFactory->create();
        $collection->getSelect()
            ->join(
                ['table1join'=>$collection->getTable('sumup_blog_category_post')],
                'main_table.entity_id = table1join.category_id',
                '*'
            )
            ->where("table1join.post_id=$idPost")
            ->where("main_table.status=1");

        $strTag = "";

        foreach ($collection as $category) {
            $url = $this->getBaseUrl();
            $strTag .= "<a href='$url{$category->getUrlKey()}' rel='category'>{$category->getName()}</a>";
        }

        $strTag = ltrim($strTag, ",");
        return $strTag;
    }

    public function getRelatedProduct()
    {
        $idPost = $this->getRequest()->getParam('id');
        $productPostCollection = $this->productPostFactory->create()->getCollection()->addFieldToFilter('blog_id', $idPost);
        $productIds = [];
        foreach ($productPostCollection as $item) {
            array_push($productIds, $item->getProductId());
        }
        $collection = $this->_productCollectionFactory->create();
        $collection->getSelect()->where('entity_id IN (?)', $productIds);
        $collection->addAttributeToSelect('*');
        $collection->setPageSize(4);
        return $collection;
    }

    public function getMediaUrl($image)
    {
        if (!$image) {
            return false;
        }
        $url = $this->storeManager->getStore()
                ->getBaseUrl(MagentoUrlInterface::URL_TYPE_MEDIA) . self::MEDIA_FOLDER;
        $url .= '/' . $image;
        return $url;
    }

    public function convertStringToArray($string)
    {
        if (strlen($string) > 1) {
            return explode(",", $string);
        }
        return [];
    }

    public function getItemImage($productId)
    {
        try {
            $_product = $this->productRepository->getById($productId);
        } catch (NoSuchEntityException $e) {
            return 'product not found';
        }
        $image_url = $this->imageHelper->init($_product, 'product_base_image')->getUrl();
        return $image_url;
    }
}
