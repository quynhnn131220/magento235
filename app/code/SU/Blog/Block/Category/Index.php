<?php

namespace SU\Blog\Block\Category;

use Magento\Framework\Registry;
use Magento\Framework\UrlInterface as MagentoUrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use SU\Blog\Model\PostFactory;
use SU\Blog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use SU\Blog\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
use SU\Blog\Model\ResourceModel\Tag\CollectionFactory as TagCollectionFactory;

class Index extends Template
{
    protected $postCollectionFactory;
    protected $categoryCollectionFactory;
    protected $registry;
    protected $context;
    protected $postFactory;
    protected $storeManager;
    protected $tagCollectionFactory;
    protected $date;
    const MEDIA_FOLDER = 'catalog/tmp/category';

    public function __construct(
        StoreManagerInterface $storeManager,
        PostCollectionFactory $postCollectionFactory,
        CategoryCollectionFactory $categoryCollectionFactory,
        Registry $registry,
        Context $context,
        PostFactory $postFactory,
        TagCollectionFactory $tagCollectionFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        array $data = []
    ) {
        $this->postCollectionFactory     = $postCollectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->registry                  = $registry;
        $this->context                   = $context;
        $this->postFactory               = $postFactory;
        $this->storeManager              = $storeManager;
        $this->tagCollectionFactory      = $tagCollectionFactory;
        $this->date = $date;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $title = __('Blog');

        /** @var Title $pageMainTitle */
        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle($title);
        }

        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb('home', [
                'label' => __('Home'),
                'title' => __('Go to Home Page'),
                'link' => $this->context->getUrlBuilder()->getBaseUrl(),
            ])->addCrumb('blog', [
                'label' => __('Blog'),
                'title' => __('Blog'),
                'link'  => $this->getBaseUrl() . 'blog',
            ]);
        }

        if ($this->getCollectionPost()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'custom.history.pager'
            )->setAvailableLimit([5 => 5, 10 => 10, 15 => 15, 20 => 20])
                ->setShowPerPage(true)->setCollection(
                    $this->getCollectionPost()
                );
            $this->setChild('pager', $pager);
            $this->getCollectionPost()->load();
        }

        return $this;
    }

    public function getCollectionPost()
    {
        $date = $this->date->gmtDate();
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;
        $collection = $this->postCollectionFactory->create();
        $collection->getSelect()
            ->join(
                ['table1join'=>$collection->getTable('sumup_blog_category_post')],
                'main_table.id = table1join.post_id',
                ['id'=>'table1join.id','id_post'=>'main_table.id']
            )
            ->join(
                ['table2join'=>$collection->getTable('sumup_blog_category_entity')],
                'table2join.entity_id = table1join.category_id',
                ['entity_id'=>'table2join.entity_id']
            )->where("table2join.status=1")
            ->where("main_table.status=1")
            ->where("main_table.publish_date_from <= ?", $date)
            ->where("main_table.publish_date_to >= ?", $date)
            ->group('main_table.id');
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);
        return $collection;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getMediaUrl($image)
    {
        if (!$image) {
            return false;
        }
        $url = $this->storeManager->getStore()
                ->getBaseUrl(MagentoUrlInterface::URL_TYPE_MEDIA) . self::MEDIA_FOLDER;
        $url .= '/' . $image;
        return $url;
    }

    public function getTagPostName($isPost)
    {
        $collection = $this->tagCollectionFactory->create();
        $collection->getSelect()
            ->join(
                ['table1join'=>$collection->getTable('sumup_blog_tag_post')],
                'main_table.id = table1join.tag_id',
                '*'
            )
            ->where("table1join.post_id=$isPost")
            ->where("main_table.status=1");

        $strTag = "";

        foreach ($collection as $tag) {
            $strTag .= "<a href='#' rel='tag'>{$tag->getName()}</a>";
        }

        $strTag = ltrim($strTag, ",");
        return $strTag;
    }

    public function getCategoryPostName($idPost)
    {
        $collection = $this->categoryCollectionFactory->create();
        $collection->getSelect()
            ->join(
                ['table1join'=>$collection->getTable('sumup_blog_category_post')],
                'main_table.entity_id = table1join.category_id',
                '*'
            )
            ->where("table1join.post_id=$idPost")
            ->where("main_table.status=1");

        $strTag = "";

        foreach ($collection as $category) {
            $url = $this->getBaseUrl();
            $strTag .= "<a href='$url{$category->getUrlKey()}' rel='category'>{$category->getName()}</a>";
        }

        $strTag = ltrim($strTag, ",");
        return $strTag;
    }
}
