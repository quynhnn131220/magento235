<?php

namespace SU\Blog\Block\Sidebar;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use SU\Blog\Model\ResourceModel\Tag\CollectionFactory as TagCollectionFactory;

class TagCloud extends Template
{
    protected $tagCollectionFactory;
    protected $context;
    protected $collection;

    public function __construct(
        TagCollectionFactory $postCollectionFactory,
        Context $context,
        array $data = []
    ) {
        $this->tagCollectionFactory = $postCollectionFactory;
        $this->context              = $context;

        parent::__construct($context, $data);
    }


    public function getCollection()
    {
        $this->collection = $this->tagCollectionFactory->create();

        return $this->collection;
    }
}
