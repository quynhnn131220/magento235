<?php

namespace SU\Blog\Block\Sidebar;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use SU\Blog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;

class CategoryTree extends Template
{
    protected $categoryCollectionFactory;
    protected $registry;
    protected $context;

    public function __construct(
        CategoryCollectionFactory $postCollectionFactory,
        Registry $registry,
        Context $context,
        array $data = []
    ) {
        $this->categoryCollectionFactory = $postCollectionFactory;
        $this->registry                  = $registry;
        $this->context                   = $context;

        parent::__construct($context, $data);
    }

    public function getTree()
    {
        return $this->categoryCollectionFactory->create();
    }

    public function isCurrent($category)
    {
        $offer_url = $_SERVER['REQUEST_URI'];
        $offer_url = trim($offer_url, '/');
        if ($offer_url == $category->getUrlKey()) {
            return true;
        }

        return false;
    }
}
