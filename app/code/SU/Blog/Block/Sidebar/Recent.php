<?php

namespace SU\Blog\Block\Sidebar;

use Magento\Framework\UrlInterface as MagentoUrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Widget\Block\BlockInterface;
use SU\Blog\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;

class Recent extends Template implements BlockInterface
{
    protected $_template = 'SU_Blog::sidebar/recent.phtml';

    const MEDIA_FOLDER = 'catalog/tmp/category';

    protected $postCollectionFactory;

    protected $context;

    public function __construct(
        PostCollectionFactory $postCollectionFactory,
        StoreManagerInterface $storeManager,
        Context $context,
        array $data = []
    ) {
        $this->postCollectionFactory = $postCollectionFactory;
        $this->context               = $context;
        $this->storeManager              = $storeManager;
        parent::__construct($context, $data);
    }

    public function getCollection()
    {
        $collection = $this->postCollectionFactory->create()
            ->addFieldToFilter('status', 1);
        return $collection;
    }

    public function getMediaUrl($image)
    {
        if (!$image) {
            return false;
        }
        $url = $this->storeManager->getStore()
                ->getBaseUrl(MagentoUrlInterface::URL_TYPE_MEDIA) . self::MEDIA_FOLDER;
        $url .= '/' . $image;
        return $url;
    }
}
