<?php

namespace SU\Blog\Helper;

use Magento\Framework\Data\Tree\Node;

class Menu extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $registry;

    protected $categoryCollectionFactory;

    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Registry $registry,
        \SU\Blog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->registry = $registry;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    public function getBlogNode($menu = null, $tree = null)
    {
        if (null == $tree) {
            $tree = new \Magento\Framework\Data\Tree();
        }

        $addedNodes = [];

        $data = [
            'name'      => "Blog",
            'id'        => 'SU-blog',
            'url'       => $this->storeManager->getStore()->getUrl('blog'),
            'is_active' => ($this->_request->getModuleName() == 'blog')
        ];

        $addedNodes[0] = new Node($data, 'id', $tree, $menu);

        $items = $this->getGroupedChilds();
        $currentCategoryId = $this->getCurrentCategory()
            ? $this->getCurrentCategory()->getId()
            : 0;

        foreach ($items as $item) {
            if ($item->getStatus() == 1) {
                $parentId = (int) $item->getParentId();

                if (!isset($addedNodes[$parentId])) {
                    continue;
                }

                $data = [
                    'name'      => $item->getName(),
                    'id'        => 'su-blog-category-' . $item->getId(),
                    'url'       => $this->storeManager->getStore()->getUrl("{$item->getUrlKey()}"),
                    'is_active' => $currentCategoryId == $item->getId()
                ];

                $addedNodes[$item->getId()] = new Node($data, 'id', $tree, $menu);
                $addedNodes[$parentId]->addChild(
                    $addedNodes[$item->getId()]
                );
            }
        }

        return $addedNodes[0];
    }

    protected function getGroupedChilds()
    {
        return $this->categoryCollectionFactory->create()
            ->getTreeOrderedArray();
    }

    protected function getCurrentCategory()
    {
        return $this->registry->registry('current_blog_category');
    }
}
