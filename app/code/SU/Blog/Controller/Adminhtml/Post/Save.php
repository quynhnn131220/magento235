<?php

namespace SU\Blog\Controller\Adminhtml\Post;

use Magento\Backend\App\Action;
use Magento\UrlRewrite\Model\UrlRewriteFactory;
use SU\Blog\Model\CategoryPostFactory;
use SU\Blog\Model\PostFactory;
use SU\Blog\Model\ProductPostFactory;
use SU\Blog\Model\TagFactory;
use SU\Blog\Model\TagPostFactory;

class Save extends Action
{
    protected $postFactory;
    protected $productPostFactory;
    protected $categoryPostFactory;
    protected $tagFactory;
    protected $tagPostFactory;
    protected $_urlRewriteFactory;

    public function __construct(
        Action\Context $context,
        PostFactory $postFactory,
        ProductPostFactory $productPostFactory,
        CategoryPostFactory $categoryPostFactory,
        TagFactory $tagFactory,
        TagPostFactory $tagPostFactory,
        UrlRewriteFactory $urlRewriteFactory
    ) {
        parent::__construct($context);
        $this->postFactory = $postFactory;
        $this->productPostFactory = $productPostFactory;
        $this->categoryPostFactory = $categoryPostFactory;
        $this->tagFactory = $tagFactory;
        $this->tagPostFactory = $tagPostFactory;
        $this->_urlRewriteFactory = $urlRewriteFactory;
    }

    public function execute()
    {
        $post = $this->postFactory->create();

        $data = $this->getRequest()->getPostValue();
        $data['title'] = trim($data['title']);
        $data['url_key'] = trim($data['url_key']);
        $gallerys = !empty($data['gallery']) ? $data['gallery'] : null;
        $galleryImg = '';
        if ($gallerys) {
            foreach ($gallerys as $gallery) {
                $galleryImg .= "{$gallery['name']},";
            }
            $galleryImg = rtrim($galleryImg, ",");
        }

        $id = !empty($data['id']) ? $data['id'] : null;
        $imageName = !empty($data['thumbnail']) ? $data['thumbnail'][0]['name'] : "default-thumbnail.jpg";

        $checkName = $post->load($data['title'], 'title');

        if (!empty($checkName->getId())) {
            if ($id) {
                $checkPostEditName = $post->load($id);
                if ($checkPostEditName->getTitle() != $data['title']) {
                    $this->getMessageManager()->addErrorMessage(__('Title post already exists.'));
                    return $this->_redirect("blog/post/edit/id/$id");
                }
            } else {
                $this->getMessageManager()->addErrorMessage(__('Title post already exists.'));
                return $this->_redirect('blog/post/add');
            }
        }

        if (!empty($data['url_key'])) {
            $url_key = trim($this->slug($data['url_key']), '-');
            $checkPostEditUrlKey = $this->postFactory->create()->getCollection()
                ->addFieldToFilter('url_key', "$url_key");

            if ($checkPostEditUrlKey->count() > 0) {
                if ($id) {
                    foreach ($checkPostEditUrlKey as $item) {
                        if ($item->getId() != $id) {
                            $this->getMessageManager()->addErrorMessage(__('Url Key post already exists.'));
                            return $this->_redirect("blog/post/edit/id/$id");
                        }
                    }
                } else {
                    $this->getMessageManager()->addErrorMessage(__('Url Key post already exists.'));
                    return $this->_redirect('blog/post/add');
                }
            }
        } else {
            $url_key = $this->slug($data['title']);
        }

        $newData = [
            'title' => $data['title'],
            'url_key' => $url_key,
            'status' => $data['status'],
            'description' => $data['description'],
            'short_description' => $data['short_description'],
            'publish_date_from' => $data['publish_date_from'],
            'publish_date_to' => $data['publish_date_to'],
            'thumbnail' => $imageName,
            'gallery' => $galleryImg
        ];

        if ($id) {
            // url rewrite
            $urlRewriteModel = $this->_urlRewriteFactory->create()->load("/blog/post/view/id/$id", 'target_path');

            $urlRewriteModel->setStoreId(1);
            $urlRewriteModel->setIsSystem(0);
            $urlRewriteModel->setIdPath(rand(1, 100000));
            $urlRewriteModel->getEntityId($id);
            $urlRewriteModel->setEntityType("Blog");
            $urlRewriteModel->setTargetPath("/blog/post/view/id/$id");
            $urlRewriteModel->setRequestPath("$url_key.html");
            $urlRewriteModel->save();

            $post->load($id);
            $this->getMessageManager()->addSuccessMessage(__('You add the post.'));
        } else {
            $this->getMessageManager()->addSuccessMessage(__('You saved the post.'));
        }
        try {
            $post->addData($newData);
            $post->save();
            $postId = $post->getId();

            // url rewrite
            if ($id == null) {
                $urlRewriteModel = $this->_urlRewriteFactory->create();
                $urlRewriteModel->setStoreId(1);
                $urlRewriteModel->setIsSystem(0);
                $urlRewriteModel->setEntityType("Blog");
                $urlRewriteModel->getEntityId($postId);
                $urlRewriteModel->setIdPath(rand(1, 100000));
                $urlRewriteModel->setTargetPath("/blog/post/view/id/$postId");
                $urlRewriteModel->setRequestPath("$url_key.html");
                $urlRewriteModel->save();
            }

            $this->saveRelatedProduct($data, $id, $postId);
            $this->saveCategoryPost($data, $id, $postId);
            $this->saveTag($data, $id, $postId);

            return $this->_redirect('blog/post/index');
        } catch (\Exception $e) {
            $this->getMessageManager()->addErrorMessage(__('You false the post.'));
        }
    }

    public function saveRelatedProduct($data, $idPost, $postSaveLastId)
    {
        $productPost = $this->productPostFactory->create();

        if ($idPost) {
            $productPostCollection= $productPost->getCollection();

            foreach ($productPostCollection as $item) {
                if ($idPost == $item['blog_id']) {
                    $productPost->load($idPost, 'blog_id');
                    $productPost->delete();
                }
            }
        }

        $productPost = $this->productPostFactory->create();
        $dataRelatedProduct = isset($data['data']['links']['product']) ? $data['data']['links']['product'] : null;

        if (isset($dataRelatedProduct)) {
            foreach ($dataRelatedProduct as $product) {
                $productPost->setData("product_id", $product['id']);
                $productPost->setData("blog_id", $postSaveLastId);
                $productPost->save();
                $productPost->setData("id", null);
            }
        }
    }

    public function saveCategoryPost($data, $idPost, $postSaveLastId)
    {
        $categoryPost = $this->categoryPostFactory->create();

        if ($idPost) {
            $categoryPostCollection= $categoryPost->getCollection();

            foreach ($categoryPostCollection as $item) {
                if ($idPost == $item['post_id']) {
                    $categoryPost->load($idPost, 'post_id');
                    $categoryPost->delete();
                }
            }
        }

        $categoryPost = $this->categoryPostFactory->create();
        $categories = isset($data['categories']) ? $data['categories'] : null;

        if (isset($categories)) {
            foreach ($categories as $category) {
                $categoryPost->setData("category_id", $category);
                $categoryPost->setData("post_id", $postSaveLastId);
                $categoryPost->save();
                $categoryPost->setData("id", null);
            }
        }
    }

    public function saveTag($data, $idPost, $postSaveLastId)
    {
        if (isset($data['tag_input'])) {
            $tag = $this->tagFactory->create();
            $tagCollection= $tag->getCollection();

            $tagPost = $this->tagPostFactory->create();
            if ($idPost) {
                $tagPostCollection= $tagPost->getCollection();

                foreach ($tagPostCollection as $item) {
                    if ($idPost == $item['post_id']) {
                        $tagPost->load($idPost, 'post_id');
                        $tagPost->delete();
                    }
                }
            }

            $dataTag = explode(',', $data['tag_input']);
            $tagPost = $this->tagPostFactory->create();
            foreach ($dataTag as $item) {
                $flag = false;

                foreach ($tagCollection as $tagItem) {
                    if ($item == $tagItem->getName()) {
                        $flag = true;
                        $a = $tagItem->getId();
                        $tagPost->setData("tag_id", $tagItem->getId());
                        $tagPost->setData("post_id", $postSaveLastId);
                        $tagPost->save();
                        $tagPost->setData("id", null);
                        break;
                    }
                }

                if ($flag == false) {
                    $tag->setData("name", $item);
                    $tag->setData("status", 1);
                    $tag->save();

                    $tagPost->setData("tag_id", $tag->getId());
                    $tagPost->setData("post_id", $postSaveLastId);
                    $tagPost->save();
                    $tagPost->setData("id", null);

                    $tag->setData("id", null);
                }
            }
        }
    }

    public function slug($str)
    {
        $str = strtolower(trim($str));
        $str = preg_replace('/[^a-z0-9-]/', '-', $str);
        $str = preg_replace('/-+/', "-", $str);
        return $str;
    }
}
