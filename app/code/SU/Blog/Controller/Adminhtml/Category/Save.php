<?php

namespace SU\Blog\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;
use SU\Blog\Model\CategoryFactory;
use Magento\UrlRewrite\Model\UrlRewriteFactory;

class Save extends Action
{
    protected $categoryFactory;
    protected $_urlRewriteFactory;

    public function __construct(Action\Context $context, CategoryFactory $categoryFactory, UrlRewriteFactory $urlRewriteFactory)
    {
        parent::__construct($context);
        $this->categoryFactory = $categoryFactory;
        $this->_urlRewriteFactory = $urlRewriteFactory;
    }

    public function execute()
    {
        $category = $this->categoryFactory->create();
        $data = $this->getRequest()->getPostValue();
        $data['name'] = trim($data['name']);
        $data['url_key'] = trim($data['url_key']);
        $data['level'] = !empty($data['parent_id']) ? $this->getLevelCategory($data['parent_id']) : 1;
        $id = !empty($data['entity_id']) ? $data['entity_id'] : null;

        $checkName = $this->categoryFactory->create()->load($data['name'], 'name');

        if (!empty($checkName->getId())) {
            if ($id) {
                $checkCategoryEditName = $category->load($id);
                if ($checkCategoryEditName->getName() != $data['name']) {
                    $this->getMessageManager()->addErrorMessage(__('Name category already exists.'));
                    return $this->_redirect("blog/category/edit/id/$id");
                }
            } else {
                $this->getMessageManager()->addErrorMessage(__('Name category already exists.'));
                return $this->_redirect('blog/category/add');
            }
        }

        if (!empty($data['url_key'])) {
            $url_key = $this->slug($data['url_key']);
            $checkUrlKey = $this->categoryFactory->create()->load($url_key, 'url_key');

            if (!empty($checkUrlKey->getId())) {
                if ($id) {
                    $checkCategoryEditUrlKey = $category->load($id);
                    if ($checkCategoryEditUrlKey->getUrlKey() != $data['url_key']) {
                        $this->getMessageManager()->addErrorMessage(__('Url Key category already exists.'));
                        return $this->_redirect("blog/category/edit/id/$id");
                    }
                } else {
                    $this->getMessageManager()->addErrorMessage(__('Url Key category already exists.'));
                    return $this->_redirect('blog/category/add');
                }
            }
        } else {
            $url_key = $this->slug($data['name']);
        }

        $newData = [
            'name' => $data['name'],
            'url_key' => $url_key,
            'status' => $data['status'],
            'level' => $data['level'],
            'parent_id' => isset($data['parent_id']) ? $data['parent_id'] : 0,
        ];
        $category = $this->categoryFactory->create();

        if ($id) {
            // url rewrite
            $urlRewriteModel = $this->_urlRewriteFactory->create()->load("/blog/category/view/id/$id", 'target_path');

            $urlRewriteModel->setStoreId(1);
            $urlRewriteModel->setIsSystem(0);
            $urlRewriteModel->setIdPath(rand(1, 100000));
            $urlRewriteModel->getEntityId($id);
            $urlRewriteModel->setEntityType("Blog");
            $urlRewriteModel->setTargetPath("/blog/category/view/id/$id");
            $urlRewriteModel->setRequestPath("$url_key");
            $urlRewriteModel->save();

            $category->load($id);
            $this->getMessageManager()->addSuccessMessage(__('You add the category.'));
        } else {
            $this->getMessageManager()->addSuccessMessage(__('You saved the category.'));
        }
        try {
            $category->addData($newData);
            $category->save();
            $categoryId = $category->getEntityId();
            // url rewrite
            if ($id == null) {
                $urlRewriteModel = $this->_urlRewriteFactory->create();
                $urlRewriteModel->setStoreId(1);
                $urlRewriteModel->setIsSystem(0);
                $urlRewriteModel->setEntityType("Blog");
                $urlRewriteModel->getEntityId($categoryId);
                $urlRewriteModel->setIdPath(rand(1, 100000));
                $urlRewriteModel->setTargetPath("/blog/category/view/id/$categoryId");
                $urlRewriteModel->setRequestPath("$url_key");
                $urlRewriteModel->save();
            }

            if ($category->getParentId() == 0) {
                $path = $category->getId();
            } else {
                $path = $category->getParentId() . '/' . $category->getId();
            }
            $category->setData("path", $path);
            $category->save();
            return $this->_redirect('blog/category/index');
        } catch (\Exception $e) {
            $this->getMessageManager()->addErrorMessage(__('You false the category.'));
        }
    }

    public function slug($str)
    {
        $str = strtolower(trim($str));
        $str = preg_replace('/[^a-z0-9-]/', '-', $str);
        $str = preg_replace('/-+/', "-", $str);
        return $str;
    }

    public function getLevelCategory($parent_id, $level=1)
    {
        if ((int)$parent_id > 0) {
            $category = $this->categoryFactory->create()->load($parent_id);
            $level = $level + 1;
            return $this->getLevelCategory($category->getParentId(), $level);
        }

        return $level;
    }
}
