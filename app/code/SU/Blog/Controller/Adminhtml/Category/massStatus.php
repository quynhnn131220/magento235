<?php

namespace SU\Blog\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\RedirectFactory;
use SU\Blog\Model\CategoryFactory;

class massStatus extends Action
{
    private $categoryFactory;
    private $resultRedirect;

    public function __construct(
        Action\Context $context,
        CategoryFactory $categoryFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context);
        $this->categoryFactory = $categoryFactory;
        $this->resultRedirect = $redirectFactory;
    }

    public function execute()
    {
        $instanceIds = $this->getRequest()->getPostValue();
        $status = (int) $this->getRequest()->getParam('status');
        $total = 0;
        $err = 0;
        foreach ($instanceIds['id'] as $id) {
            $changeStatusPost = $this->categoryFactory->create()->load($id);
            try {
                $changeStatusPost->setData('status', $status);
                $changeStatusPost->save();
                $total++;
            } catch (LocalizedException $exception) {
                $err++;
            }
        }

        if ($total) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been changed status.', $total)
            );
        }

        if ($err) {
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been changed status. Please see server logs for more details.',
                    $err
                )
            );
        }
        return $this->resultRedirect->create()->setPath('blog/category/index');
    }
}
