<?php

namespace SU\Blog\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\RedirectFactory;
use SU\Blog\Model\CategoryFactory;
use SU\Blog\Model\ResourceModel\Category\CollectionFactory;

class Delete extends Action
{
    private $categoryFactory;
    private $collectionFactory;
    private $resultRedirect;

    public function __construct(
        Action\Context $context,
        CategoryFactory $categoryFactory,
        CollectionFactory $collectionFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context);
        $this->categoryFactory = $categoryFactory;
        $this->collectionFactory = $collectionFactory;
        $this->resultRedirect = $redirectFactory;
    }

    public function execute()
    {
        $instanceIds = $this->getRequest()->getPostValue();
        $total = 0;
        $err = 0;
        foreach ($instanceIds['id'] as $id) {
            $deleteCategory = $this->categoryFactory->create()->load($id);
            try {
                $deleteCategory->delete();
                $total++;
            } catch (LocalizedException $exception) {
                $err++;
            }
        }

        if ($total) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $total)
            );
        }

        if ($err) {
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been deleted. Please see server logs for more details.',
                    $err
                )
            );
        }
        return $this->resultRedirect->create()->setPath('blog/category/index');
    }
}
