<?php

namespace SU\Blog\Model\Config;

use Magento\Catalog\Helper\Image as ImageHelper;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\UrlInterface as MagentoUrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use SU\Blog\Model\CategoryPostFactory;
use SU\Blog\Model\ProductPostFactory;
use SU\Blog\Model\ResourceModel\Post\CollectionFactory;
use SU\Blog\Model\TagFactory;
use SU\Blog\Model\TagPostFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $_loadedData;
    protected $collection;
    protected $storeManager;
    protected $categoryPostFactory;
    protected $tagPostFactory;
    protected $tagFactory;
    protected $productFactory;
    protected $productPostFactory;

    const MEDIA_FOLDER = 'catalog/tmp/category';

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        StoreManagerInterface $storeManager,
        CollectionFactory $collectionFactory,
        CategoryPostFactory $categoryPostFactory,
        TagPostFactory $tagPostFactory,
        TagFactory $tagFactory,
        ProductPostFactory $productPostFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        Status $status,
        ImageHelper $imageHelper,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->storeManager = $storeManager;
        $this->categoryPostFactory = $categoryPostFactory->create();
        $this->tagPostFactory = $tagPostFactory->create();
        $this->tagFactory = $tagFactory->create();
        $this->productFactory = $productFactory->create();
        $this->productPostFactory = $productPostFactory->create();
        $this->status         = $status;
        $this->imageHelper    = $imageHelper;
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }

        $items = $this->collection->getItems();
        foreach ($items as $post) {
            $post = $post->load($post->getId()); //temporary fix
            $data = $post->getData();

            /* Prepare Featured Image */
            $map = [
                'thumbnail' => 'getThumbnail',
            ];
            foreach ($map as $key => $method) {
                if (isset($data[$key])) {
                    $name = $data[$key];
                    unset($data[$key]);
                    $data[$key][0] = [
                        'name' => $name,
                        'url' => $this->getMediaUrl($post->$method()),
                        'type' => 'image',
                    ];
                }
            }

            /* Prepare Gallery Image */
            if (strlen($post->getGallery()) > 1) {
                $mapGallery = [
                    'gallery' => 'getGallery',
                ];
                foreach ($mapGallery as $key => $method) {
                    if (isset($data[$key])) {
                        $name = $data[$key];
                        unset($data[$key]);
                        $string = $post->$method();
                        $arr = explode(",", $string);

                        foreach ($arr as $i => $item) {
                            $data[$key][$i] = [
                                'name' => $item,
                                'url' => $this->getMediaUrl($item),
                                'type' => 'image',
                            ];
                        }
                    }
                }
            }

            /* Prepare related products */
            $productPostCollection = $this->productPostFactory->getCollection()->addFieldToFilter('blog_id', $post->getId());

            $items = [];
            foreach ($productPostCollection as $productPost) {
                $product = $this->productFactory->load($productPost->getProductId());
                $items[] = [
                    'id'        => $product->getId(),
                    'name'      => $product->getName(),
                    'status'    => $this->status->getOptionText($product->getStatus()),
                    'thumbnail' => $this->imageHelper->init($product, 'product_listing_thumbnail')->getUrl(),
                ];
            }
            $data['data']['links']['product'] = $items;

            /* Prepare Category */
            $data['categories'] = [];
            $categoryPostCollection = $this->categoryPostFactory->getCollection()->addFieldToFilter('post_id', $post->getId());
            foreach ($categoryPostCollection as $categoryPost) {
                array_push($data['categories'], $categoryPost->getCategoryId());
            }

            /* Prepare Tag */
            $data['tag_input'] = [];
            $tagPostCollection = $this->tagPostFactory->getCollection()->addFieldToFilter('post_id', $post->getId());
            foreach ($tagPostCollection as $tagPost) {
                $tag = $this->tagFactory->load($tagPost->getTagId());
                array_push($data['tag_input'], $tag->getName());
            }

            $this->_loadedData[$post->getId()] = $data;
        }
        return $this->_loadedData;
    }

    public function getMediaUrl($image)
    {
        if (!$image) {
            return false;
        }
        $url = $this->storeManager->getStore()
                ->getBaseUrl(MagentoUrlInterface::URL_TYPE_MEDIA) . self::MEDIA_FOLDER;
        $url .= '/' . $image;
        return $url;
    }
}
