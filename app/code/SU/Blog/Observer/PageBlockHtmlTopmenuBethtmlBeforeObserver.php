<?php

namespace SU\Blog\Observer;

use Magento\Framework\Event\ObserverInterface;

class PageBlockHtmlTopmenuBethtmlBeforeObserver implements ObserverInterface
{
    protected $menuHelper;

    public function __construct(
        \SU\Blog\Helper\Menu $menuHelper
    ) {
        $this->menuHelper = $menuHelper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $menu = $observer->getMenu();
        $tree = $menu->getTree();

        $blogNode = $this->menuHelper->getBlogNode($menu, $menu->getTree());
        if ($blogNode) {
            $menu->addChild($blogNode);
        }
    }
}
